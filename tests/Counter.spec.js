import { Counter } from "../src/Counter"


describe('class Counter', () => {
let instance;
beforeEach(() => {
    instance = new Counter;
});

    it('should a new instance', ( ) =>{

        expect(instance).toBeDefined();
        expect(instance.value).toBe(0);
    });

    it('Sould decrement counter on decrement call', () => {

        instance.decrement();

        expect(instance.value).toBe(-1);

    });
    it('should increment', () => {

        instance.increment();

        expect(instance.value).toBe(1);
    });
    it('should reset', () => {

        instance.reset = 3;

        expect(instance.value).toBe(0);
    });




});